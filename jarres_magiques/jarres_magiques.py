import random as rd

NB_JARRES = 5

## Valeur a trouver ##

def gen_number(niveau_level):
    num_serpent = []
    nb_len = len(num_serpent)

    for nb in range(niveau_level):
        nb_temp = rd.randrange(NB_JARRES) + 1
        #print(nb_temp)
        while not verif_doublon(num_serpent, nb_temp):
            nb_temp = rd.randrange(NB_JARRES)
            #print(verif_doublon(num_serpent, nb_temp))
            #print("nb changer :",nb_temp)

        num_serpent.append(nb_temp)
    #print(num_serpent)
    return num_serpent

## verif doublon ##

def verif_doublon(num_serpent, nb_temp):
    verif = True
    for nb in num_serpent :
        if nb_temp == nb :
            verif = False
    return verif

## input ##

def pick_number():
    number_propose = int(input("Quel jarres choisie tu ? \n"))
    return number_propose

def pick_level():
    nb_level = int(input("tu as le choix entre 3 niveaux le quel choisie tu ? "))
    return nb_level

## verif input ##

def verif_input(num_serpent, number_propose):

    ## check defaite
    resultat = True
    for i in range(len(num_serpent)):
        #print(number_propose, "==", num_serpent[i])
        if number_propose == num_serpent[i]:
           resultat = False
           break
        else:
           resultat = True
        #print(resultat)
    return resultat


## fonction affichage ##
def print_jarres():

    for jarres in range(NB_JARRES) :
        print(jarres+1,"- La jarre",jarres+1)

def print_vict():
    print("Gagné")

def print_def():
    print("piege")

def print_trousseau(nb_cle):
    print("tu as", nb_cle, "clé")




## boucle ##

def boucle():

    nb_level = pick_level()
    num_serpent = gen_number(nb_level)

    nb_cle, nb = 1, 0

    while nb_cle > 0:
        print_jarres()
        print_trousseau(nb_cle)
        nb = pick_number()

        if verif_input(num_serpent, nb):
            print_vict()
            nb_cle +=1
        else:
            print_def()
            nb_cle -=1
            num_serpent = gen_number(nb_level)

        if nb_cle == 3:
            print("Tu deviens roi du temple")
            break
        if nb_cle == 0:
            print("tu n'as plu de clé")



## main ##

if __name__ == '__main__':
    boucle()
