import random as rd

nb_try = 0
victoire = False
defaite = False

## Valeur a trouver ##

def gen_number():
    number_guess = rd.randrange(10)
    return number_guess

## input ##

def pick_number():
    number_propose = int(input("try a number : \n"))
    return number_propose


## verif input ##

def verif_input(number_guess, number_propose):
    global victoire,defaite, nb_try
    ## check voctoire
    if number_propose == number_guess:
        victoire = True
    else:
        ## check defaite
        if nb_try >= 9:
            defaite = True
        else:
            if number_propose < number_guess:
                print("le nombre est superieur")
                nb_try += 1
            else :
                print("le nombre est inferieur")
                nb_try += 1


## fonction affichage ##
def print_try():
    print("il vous reste ", 10- nb_try, "essais")

def print_vict():
    print("bravo")

def print_def():
    print("perdu")


## boucle ##

def boucle():
    number_guess = gen_number()

    while not (victoire or defaite):
        print_try()
        number_propose = pick_number()
        verif_input(number_guess, number_propose)

    if victoire:
        print_vict()
    else:
        print_def()

## main ##

boucle()
